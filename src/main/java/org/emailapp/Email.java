package org.emailapp;

import java.util.Scanner;

public class Email {

    private String firstName;
    private String lastName;
    private String password;

    private String email;
    private String department;
    private int mailboxCapacity = 500;
    private int defaultPasswordLength = 8;
    private String alternateEmail;
    private String companySuffix = "company.com";

    // Constructors to get first and last names
    public Email(String firstName, String lastName){
        this.firstName = firstName;
        this.lastName = lastName;
        System.out.println("Email created " + this.firstName + " " + this.lastName);

        // Call method asking for the department and return the department
        this.department = setDepartment();
        // System.out.println("Department chosen: " + this.department);
        this.password = randomPassword(defaultPasswordLength);
        this.email = firstName.toLowerCase() + "." + lastName.toLowerCase() + "@" + department + "." + companySuffix;
        // System.out.println("Your email address is: " + this.email);


    }


    // Generate random password
    private String randomPassword(int lenght) {
        String passwordSet = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890$%&@*+";
        char[] password = new char[lenght];
        for (int i=0; i<lenght; i++){
            int randVal = (int) (Math.random() * passwordSet.length());
            password[i] = passwordSet.charAt(randVal);
        }
        return new String(password);

    }


    // Ask for department
    private String setDepartment(){
        System.out.println("Enter the Department\n1 for Engineering\n2 for I.T.\n Please Enter department code:");
        Scanner in = new Scanner(System.in);
        int deptChoice = in.nextInt();
        if (deptChoice == 1) {
            return "engineering";
        }
        else if(deptChoice == 2) {
            return "it";
        }
        else {
            return "No Department chosen";
        }
    }

    // Set mailbox capacity
    public void setMailboxCapacity(int capacity){
        this.mailboxCapacity = capacity;
    }

    // Set alternate email
    public void setAlternateEmail(String altEmail){
        this.alternateEmail = altEmail;
    }

    // Change password
    public void changePassword(String newPassword){
        this.password = newPassword;
    }


    public int getMailboxCapacity() { return mailboxCapacity; }
    public String getAlternateEmail() { return alternateEmail; }

    public String showInfo() {
        return "Display Name: " + firstName + " " + lastName + "\n" + "Email address: " + " " + this.email + "\n" + "Mailbox Capacity: " + " " + this.mailboxCapacity + "mb";
    }


}
